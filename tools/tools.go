//go:build tools

// https://github.com/golang/go/issues/25922
package tools

import (
	_ "gitlab.com/matthewhughes/go-cov/cmd/go-cov"
)
