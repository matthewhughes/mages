package mages

import (
	"fmt"
	"os"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

const (
	covFile        = "coverage.out"
	skippedCovFile = "go-cov.out"
	covCmd         = "gitlab.com/matthewhughes/go-cov/cmd/go-cov"
)

func Test() error { //go-cov:skip
	return sh.RunV( //nolint:wrapcheck
		"go",
		"test",
		"-coverpkg=./...",
		fmt.Sprintf("-coverprofile=%s", covFile),
		"./...",
	)
}

func AddSkips() error { //go-cov:skip
	mg.Deps(Test)

	skippedProfile, err := sh.Output("go", "run", covCmd, "add-skips", covFile)
	if err != nil {
		return err //nolint:wrapcheck
	}

	if err := os.WriteFile(skippedCovFile, []byte(skippedProfile), 0o600); err != nil {
		return fmt.Errorf("writing skipped profiles to '%s': %w", skippedProfile, err)
	}
	return nil
}

func ReportCovHTML() error { //go-cov:skip
	mg.Deps(AddSkips)

	return sh.RunV( //nolint:wrapcheck
		"go",
		"tool",
		"cover",
		"-html",
		skippedCovFile,
	)
}

func CheckCov() error { //go-cov:skip
	mg.Deps(AddSkips)

	return sh.RunV( //nolint:wrapcheck
		"go",
		"run",
		covCmd,
		"report",
		"--fail-under",
		"100",
		skippedCovFile,
	)
}
