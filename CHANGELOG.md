# Changelog

## 0.2.0 - 2024-05-22

### Added

  - Add command to report coverage as HTML

## 0.1.0 - 2024-05-22

Initial release
