# `mages`

A collection of [`magefile`](https://magefile.org) targets for my personal use.
It provides the following:

  - `test`: run `go test ./...` with coverage
  - `addskips`: run `go-cov` over the coverage file generated from testing
  - `reportcovhtml`: run `go tool cover -html` over the coverage file with skips
  - `checkcov`: ensure the test coverage (with skips added) is 100%

## Setup

Your `.gitignore` should include the following:

``` gitignore
# coverage artifacts
/coverage.out
/go-cov.out
```

Your module should include
[`gitlab.com/matthewhughes/go-cov`](https://gitlab.com/matthewhughes/go-cov) as
a dependency, e.g. in a `tools.go`:

``` go
//go:build tools

// https://github.com/golang/go/issues/25922
package tools

import (
	_ "gitlab.com/matthewhughes/go-cov/cmd/go-cov"
)
```

Finally add a `magefile.go`:

``` go
//go:build mage

package main

import (
	//mage:import
	_ "gitlab.com/matthewhughes/mages"
)
```
